from datetime import datetime
import json
from django.http import JsonResponse
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time

# from attendees_microservice.common.json import DateEncoder


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO


# Declare a function to update the AccountVO object (ch, method, properties, body)
def update_account_vo(ch, method, properties, body):
    content = json.loads(body)
    #   content = load the json in body
    first_name = content["first_name"]
    #   first_name = content["first_name"]
    last_name = content["last_name"]
    #   last_name = content["last_name"]
    email = content["email"]
    #   email = content["email"]
    is_active = content["is_active"]
    #   is_active = content["is_active"]
    updated_string = content["updated"]
    #   updated_string = content["updated"]
    # updated = datetime.date.fromisoformat
    updated = datetime.fromisoformat(updated_string)
    #   updated = convert updated_string from ISO string to datetime
    if is_active:
        account = AccountVO.objects.create(**content)
        # return JsonResponse(account, safe=False)
    else:
        count, _ = AccountVO.filter(id).delete()
        # return JsonResponse({"deleted": count > 0})
    # if is_active:
    #     data = {
    #         "first_name": first_name,
    #         "last-name": last_name,
    #         "email": email,
    #     }
    #     data2 = {
    #         "first_name": "not provided",
    #         "last-name": "not provided",
    #         "email": "not provided",
    #     }
    #     print("data", data)
    #     obj, created = AccountVO.objects.update_or_create(data)

    # else:
    #     account_email = AccountVO.objects.get(email=email)
    #     account_email.delete()


#   otherwise:
#       Delete the AccountVO object with the specified email, if it exists


# Based on the reference code at
#   https://github.com/rabbitmq/rabbitmq-tutorials/blob/master/python/receive_logs.py
# infinite loop   #while True


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        #       create the pika connection parameters

        connection = pika.BlockingConnection(parameters)
        #       create a blocking connection with the parameters

        channel = connection.channel()
        #       open a channel

        channel.exchange_declare(
            exchange="account_info", exchange_type="fanout"
        )
        #       declare a fanout exchange named "account_info"

        result = channel.queue_declare(queue="", exclusive=True)
        #       declare a randomly-named queue
        queue_name = result.method.queue
        #       get the queue name of the randomly-named queue

        channel.queue_bind(exchange="account_info", queue=queue_name)
        #       bind the queue to the "account_info" exchange

        channel.basic_consume(
            queue="",
            on_message_callback=update_account_vo,
            auto_ack=True,
        )
        #       do a basic_consume for the queue name that calls
        #           function above

        channel.start_consuming()
    #       tell the channel to start consuming

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
#   except AMQPConnectionError
#       print that it could not connect to RabbitMQ
#       have it sleep for a couple of seconds
