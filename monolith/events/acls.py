from pydoc import describe
import requests
import json
from .keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY
import math


def pexels_pics(city, state):
    url = "https://api.pexels.com/v1/search?query=" + city + "," + state

    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    data = json.loads(response.content)

    photo_url = data["photos"][0]["src"]["original"]

    return {"picture_url": photo_url}


def get_weather_data(city, state):
    url = (
        "http://api.openweathermap.org/geo/1.0/direct?q="
        + city
        + ","
        + state
        + ",US&limit=5&appid="
        + OPEN_WEATHER_API_KEY
    )

    response = requests.get(url)

    log_lat_data = json.loads(response.content)[0]

    lon = log_lat_data["lon"]
    lat = log_lat_data["lat"]

    # return {"longitude": lon, "latitude": lat}

    url2 = (
        "https://api.openweathermap.org/data/2.5/weather?lat="
        + str(lat)
        + "&lon="
        + str(lon)
        + "&appid="
        
        + OPEN_WEATHER_API_KEY
    )
    response2 = requests.get(url2)
    weather_data = json.loads(response2.content)
    kevline = weather_data["main"]["temp"]
    temp = (kevline - 273.15) * 9/5 + 32 
    temp = math.floor(temp)
    describe_weather = weather_data["weather"][0]["description"]
   
    print("WEATHER", temp, describe_weather)

    return {"temp": temp, "describe_weather": describe_weather}



